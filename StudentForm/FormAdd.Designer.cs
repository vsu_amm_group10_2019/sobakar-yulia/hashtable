﻿
namespace StudentForm
{
    partial class FormAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.labelID = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.comboBoxBD = new System.Windows.Forms.ComboBox();
            this.comboBoxPrg = new System.Windows.Forms.ComboBox();
            this.comboBoxHst = new System.Windows.Forms.ComboBox();
            this.ComboBoxWeb = new System.Windows.Forms.ComboBox();
            this.ComboBoxLaw = new System.Windows.Forms.ComboBox();
            this.labelSession = new System.Windows.Forms.Label();
            this.labelBD = new System.Windows.Forms.Label();
            this.labelData = new System.Windows.Forms.Label();
            this.labelPrg = new System.Windows.Forms.Label();
            this.labelHst = new System.Windows.Forms.Label();
            this.labelWeb = new System.Windows.Forms.Label();
            this.labelLaw = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.labelFail = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(9, 64);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(156, 20);
            this.textBoxID.TabIndex = 0;
            this.textBoxID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxID_KeyPress);
            // 
            // labelID
            // 
            this.labelID.AutoSize = true;
            this.labelID.Location = new System.Drawing.Point(6, 38);
            this.labelID.Name = "labelID";
            this.labelID.Size = new System.Drawing.Size(156, 13);
            this.labelID.TabIndex = 1;
            this.labelID.Text = "Номер студенческого билета";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(9, 122);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(150, 20);
            this.textBoxName.TabIndex = 2;
            this.textBoxName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxName_KeyPress);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(6, 97);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(37, 13);
            this.labelName.TabIndex = 3;
            this.labelName.Text = "ФИО ";
            // 
            // comboBoxBD
            // 
            this.comboBoxBD.FormattingEnabled = true;
            this.comboBoxBD.Items.AddRange(new object[] {
            "5",
            "4",
            "3",
            "2"});
            this.comboBoxBD.Location = new System.Drawing.Point(9, 205);
            this.comboBoxBD.Name = "comboBoxBD";
            this.comboBoxBD.Size = new System.Drawing.Size(121, 21);
            this.comboBoxBD.TabIndex = 4;
            // 
            // comboBoxPrg
            // 
            this.comboBoxPrg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPrg.FormattingEnabled = true;
            this.comboBoxPrg.Items.AddRange(new object[] {
            "5",
            "4",
            "3",
            "2"});
            this.comboBoxPrg.Location = new System.Drawing.Point(9, 270);
            this.comboBoxPrg.Name = "comboBoxPrg";
            this.comboBoxPrg.Size = new System.Drawing.Size(121, 21);
            this.comboBoxPrg.TabIndex = 5;
            // 
            // comboBoxHst
            // 
            this.comboBoxHst.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHst.FormattingEnabled = true;
            this.comboBoxHst.Items.AddRange(new object[] {
            "5",
            "4",
            "3",
            "2"});
            this.comboBoxHst.Location = new System.Drawing.Point(9, 329);
            this.comboBoxHst.Name = "comboBoxHst";
            this.comboBoxHst.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHst.TabIndex = 6;
            // 
            // ComboBoxWeb
            // 
            this.ComboBoxWeb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxWeb.FormattingEnabled = true;
            this.ComboBoxWeb.Items.AddRange(new object[] {
            "зачет",
            "незачет"});
            this.ComboBoxWeb.Location = new System.Drawing.Point(189, 205);
            this.ComboBoxWeb.Name = "ComboBoxWeb";
            this.ComboBoxWeb.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxWeb.TabIndex = 7;
            // 
            // ComboBoxLaw
            // 
            this.ComboBoxLaw.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxLaw.FormattingEnabled = true;
            this.ComboBoxLaw.Items.AddRange(new object[] {
            "зачет",
            "незачет"});
            this.ComboBoxLaw.Location = new System.Drawing.Point(189, 270);
            this.ComboBoxLaw.Name = "ComboBoxLaw";
            this.ComboBoxLaw.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxLaw.TabIndex = 8;
            // 
            // labelSession
            // 
            this.labelSession.AutoSize = true;
            this.labelSession.Location = new System.Drawing.Point(105, 164);
            this.labelSession.Name = "labelSession";
            this.labelSession.Size = new System.Drawing.Size(106, 13);
            this.labelSession.TabIndex = 9;
            this.labelSession.Text = "Результаты сессии";
            this.labelSession.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelBD
            // 
            this.labelBD.AutoSize = true;
            this.labelBD.Location = new System.Drawing.Point(6, 189);
            this.labelBD.Name = "labelBD";
            this.labelBD.Size = new System.Drawing.Size(74, 13);
            this.labelBD.TabIndex = 10;
            this.labelBD.Text = "Базы данных";
            // 
            // labelData
            // 
            this.labelData.AutoSize = true;
            this.labelData.Location = new System.Drawing.Point(124, 9);
            this.labelData.Name = "labelData";
            this.labelData.Size = new System.Drawing.Size(87, 13);
            this.labelData.TabIndex = 11;
            this.labelData.Text = "Личные данные";
            this.labelData.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelPrg
            // 
            this.labelPrg.AutoSize = true;
            this.labelPrg.Location = new System.Drawing.Point(8, 254);
            this.labelPrg.Name = "labelPrg";
            this.labelPrg.Size = new System.Drawing.Size(108, 13);
            this.labelPrg.TabIndex = 12;
            this.labelPrg.Text = "Программирование";
            // 
            // labelHst
            // 
            this.labelHst.AutoSize = true;
            this.labelHst.Location = new System.Drawing.Point(6, 313);
            this.labelHst.Name = "labelHst";
            this.labelHst.Size = new System.Drawing.Size(50, 13);
            this.labelHst.TabIndex = 13;
            this.labelHst.Text = "История";
            // 
            // labelWeb
            // 
            this.labelWeb.AutoSize = true;
            this.labelWeb.Location = new System.Drawing.Point(186, 189);
            this.labelWeb.Name = "labelWeb";
            this.labelWeb.Size = new System.Drawing.Size(74, 13);
            this.labelWeb.TabIndex = 14;
            this.labelWeb.Text = "Web-верcтка";
            // 
            // labelLaw
            // 
            this.labelLaw.AutoSize = true;
            this.labelLaw.Location = new System.Drawing.Point(186, 254);
            this.labelLaw.Name = "labelLaw";
            this.labelLaw.Size = new System.Drawing.Size(81, 13);
            this.labelLaw.TabIndex = 15;
            this.labelLaw.Text = "Правоведение";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(127, 380);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(72, 39);
            this.buttonOk.TabIndex = 16;
            this.buttonOk.Text = "ОК";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // labelFail
            // 
            this.labelFail.AutoSize = true;
            this.labelFail.Location = new System.Drawing.Point(186, 313);
            this.labelFail.Name = "labelFail";
            this.labelFail.Size = new System.Drawing.Size(0, 13);
            this.labelFail.TabIndex = 17;
            this.labelFail.Visible = false;
            // 
            // FormAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 431);
            this.Controls.Add(this.labelFail);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelLaw);
            this.Controls.Add(this.labelWeb);
            this.Controls.Add(this.labelHst);
            this.Controls.Add(this.labelPrg);
            this.Controls.Add(this.labelData);
            this.Controls.Add(this.labelBD);
            this.Controls.Add(this.labelSession);
            this.Controls.Add(this.ComboBoxLaw);
            this.Controls.Add(this.ComboBoxWeb);
            this.Controls.Add(this.comboBoxHst);
            this.Controls.Add(this.comboBoxPrg);
            this.Controls.Add(this.comboBoxBD);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.labelID);
            this.Controls.Add(this.textBoxID);
            this.Name = "FormAdd";
            this.Text = "Данные о студенте";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label labelID;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.ComboBox comboBoxBD;
        private System.Windows.Forms.ComboBox comboBoxPrg;
        private System.Windows.Forms.ComboBox comboBoxHst;
        private System.Windows.Forms.ComboBox ComboBoxWeb;
        private System.Windows.Forms.ComboBox ComboBoxLaw;
        private System.Windows.Forms.Label labelSession;
        private System.Windows.Forms.Label labelBD;
        private System.Windows.Forms.Label labelData;
        private System.Windows.Forms.Label labelPrg;
        private System.Windows.Forms.Label labelHst;
        private System.Windows.Forms.Label labelWeb;
        private System.Windows.Forms.Label labelLaw;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Label labelFail;
    }
}