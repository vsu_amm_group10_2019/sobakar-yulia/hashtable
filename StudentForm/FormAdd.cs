﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentForm
{
    public partial class FormAdd : Form

    {
        public Student Student { get; } = new Student();
        FormState formState;
        public bool okClose;
        public FormAdd(FormState formState, Student student = null)
        {
            InitializeComponent();
            this.formState = formState;
            okClose = false;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        textBoxName.ReadOnly = false;
                        textBoxID.ReadOnly = false;
                        comboBoxBD.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBoxPrg.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBoxHst.DropDownStyle = ComboBoxStyle.DropDownList;
                        ComboBoxWeb.DropDownStyle = ComboBoxStyle.DropDownList;
                        ComboBoxLaw.DropDownStyle = ComboBoxStyle.DropDownList;
                        labelFail.Visible = false;
                        Text = "Add student";
                        break;
                    }
                case FormState.EDIT:
                    {
                        textBoxID.ReadOnly = true;
                        textBoxID.Text = Convert.ToString(student.ID);
                        textBoxName.Text = student.FullName;
                        comboBoxBD.Text = student.Exams[0].ToString();
                        comboBoxPrg.Text = student.Exams[1].ToString();
                        comboBoxHst.Text = student.Exams[2].ToString();
                        ComboBoxWeb.Text = student.Exams[3].ToString();
                        ComboBoxLaw.Text = student.Exams[4].ToString(); 

                        comboBoxBD.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBoxPrg.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBoxHst.DropDownStyle = ComboBoxStyle.DropDownList;
                        ComboBoxWeb.DropDownStyle = ComboBoxStyle.DropDownList;
                        ComboBoxLaw.DropDownStyle = ComboBoxStyle.DropDownList;
                        labelFail.Visible = false;
                        Text = "Edit student";
                        break;
                    }

                case FormState.DELETE:
                case FormState.SEARCH:
                    {
                        textBoxID.ReadOnly = false;
                        textBoxName.ReadOnly = true;
                        comboBoxBD.DropDownStyle = ComboBoxStyle.DropDownList; 
                        comboBoxPrg.DropDownStyle = ComboBoxStyle.DropDownList; 
                        comboBoxHst.DropDownStyle = ComboBoxStyle.DropDownList; 
                        ComboBoxWeb.DropDownStyle = ComboBoxStyle.DropDownList;
                        ComboBoxLaw.DropDownStyle = ComboBoxStyle.DropDownList;
                        labelFail.Visible = false;
                        Text = "Search student";
                        break;
                    }

                case FormState.DISPLAY:
                    {
                        textBoxID.ReadOnly = true;
                        textBoxID.Text = student.ID.ToString();
                        textBoxName.Text = student.FullName;
                        comboBoxBD.Text = student.Exams[0].ToString();
                        comboBoxPrg.Text = student.Exams[1].ToString();
                        comboBoxHst.Text = student.Exams[2].ToString();
                        ComboBoxWeb.Text = student.Exams[3].ToString();
                        ComboBoxLaw.Text = student.Exams[3].ToString();

                        textBoxName.ReadOnly = true;
                        comboBoxBD.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBoxPrg.DropDownStyle = ComboBoxStyle.DropDownList;
                        comboBoxHst.DropDownStyle = ComboBoxStyle.DropDownList;
                        ComboBoxWeb.DropDownStyle = ComboBoxStyle.DropDownList;
                        ComboBoxLaw.DropDownStyle = ComboBoxStyle.DropDownList;
                        labelFail.Visible = true;
                        labelFail.Text = $"кол-во хвостов: {Student.CounterFailedExam(student.Exams).ToString()}";
                        Text = "Show student";
                        break;
                    }
            }
        }

        public Student GetStudent()
        {
            List<string> exams = new List<string>();
                exams.Add(comboBoxBD.Text);
                exams.Add(comboBoxPrg.Text);
                exams.Add(comboBoxHst.Text);
                exams.Add(ComboBoxWeb.Text);
                exams.Add(ComboBoxLaw.Text);
                return new Student(textBoxName.Text, Convert.ToInt32(textBoxID.Text), exams); 
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
          if (formState == FormState.ADD || formState == FormState.EDIT)
            {
                if (textBoxID.Text != "" && textBoxName.Text != ""
                && comboBoxBD.Text != "" && comboBoxPrg.Text != "" && comboBoxHst.Text != "" && comboBoxHst.Text != ""
                && ComboBoxWeb.Text != "" && ComboBoxLaw.Text != "" ) 
                {
                    Student.ID = Convert.ToInt32(textBoxID.Text);
                    okClose = true;
                    this.Close();
                } else
                {
                    MessageBox.Show("Данные заполнены некорректно!");
                } 
            
           } else if (textBoxID.Text != "")
                {
                    Student.ID = Convert.ToInt32(textBoxID.Text);
                    okClose = true;
                    this.Close();
                } else
                {
                    MessageBox.Show("Введите номер студенческого!");
           } 
        }

        private void textBoxID_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8)  
            {
                e.Handled = true;
            }
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e)
        {
            char letter = e.KeyChar;
            if (!Char.IsLetter(letter) && e.KeyChar != (char)Keys.Space && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true; 
            }
        }
    }
}
