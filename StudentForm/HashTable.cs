﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentForm
{
    class HashTable
    {
        private Element[] Table { get; }
        public int Size { get; }
        public HashTable(int size = 100) // Конструктор
        {
            Size = size;
            Table = new Element[size];
        }

        public bool Add(Student student)
        {
            if (Find(student.ID) != null)
            {
                return false;
            }
            int index = student.ID % Size;
            if (Table[index] == null || Table[index].Deleted)
            {
                Table[index] = new Element()
                {
                    Student = student
                };
                return true;
            }
            int j = 1;
            for (int i = index + j + j * j; i < Size; i += j + j * j)
            {
                if (Table[i] == null || Table[i].Deleted) 
                {
                    Table[i] = new Element()
                    {
                        Student = student
                    };
                    return true;
                }
                ++j;
            }

            j = 0;
            for (int i = 0; i < index; i += j + j * j)
            {
                if (Table[i] == null || Table[i].Deleted)
                {
                    Table[i] = new Element()
                    {
                        Student = student
                    };
                    return true;
                }
                ++j;
            }

            return false;
        }

        public Student Find(int id)
        {
            int index = id % Size;

            if (Table[index] == null)
            {
                return null;
            }

            if (!Table[index].Deleted && Table[index].Student.ID == id)
            {
                return Table[index].Student;
            }
            int j = 1;
            for (int i = index + j + j * j; i < Size; i += j + j * j)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    return Table[i].Student;
                }

                ++j;
            }

            j = 0;
            for (int i = 0; i < index; i += j + j * j)
            {
                if (Table[i] == null)
                {
                    return null;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    return Table[i].Student;
                }
                ++j;
            }
            return null;
        }

        public void Delete(int id)
        {
            int index = id % Size;

            if (Table[index] == null)
            {
                return;
            }

            if (!Table[index].Deleted && Table[index].Student.ID == id)
            {
                Table[index].Deleted = true;
                return;
            }
            int j = 1;
            for (int i = index + j + j * j; i < Size; i += j + j * j)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    Table[index].Deleted = true;
                    return;
                }

                ++j;
            }

            j = 0;
            for (int i = 0; i < index; i += j + j * j)
            {
                if (Table[i] == null)
                {
                    return;
                }
                if (!Table[i].Deleted && Table[i].Student.ID == id)
                {
                    Table[index].Deleted = true;
                    return;
                }
                ++j;
            }
        }

        public void Clear()
        {
            for (int i = 0; i < Size; i++)
            {
                Table[i] = null;
            }
        }

        public List<Student> GetData()
        {
            List<Student> list = new List<Student>();
            foreach (Element elem in Table)
            {
                if (elem != null && !elem.Deleted)
                {
                    list.Add(elem.Student);
                }
            }
            return list;
        }

        class Element
        {
            public Student Student { get; set; }
            public bool Deleted { get; set; } = false;
        }
    }
}

