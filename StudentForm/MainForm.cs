﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.ComponentModel.Design;

namespace StudentForm
{
    public partial class MainForm : Form
    {
        private string FileName { get; set; } = null;
        private HashTable table;
        private Student student;
        public MainForm()
        {
            InitializeComponent();
            table = new HashTable(100);
        }

        private void Redraw()
        {
            dataTableStudent.Rows.Clear();
            List<Student> list = table.GetData();
            dataTableStudent.RowCount = list.Count;
            for (int i = 0; i < dataTableStudent.RowCount; i++)
            {
                dataTableStudent.Rows[i].Cells[0].Value = list[i].ID;
                dataTableStudent.Rows[i].Cells[1].Value = list[i].FullName;
                dataTableStudent.Rows[i].Cells[2].Value = list[i].Exams[0];
                dataTableStudent.Rows[i].Cells[3].Value = list[i].Exams[1];
                dataTableStudent.Rows[i].Cells[4].Value = list[i].Exams[2];
                dataTableStudent.Rows[i].Cells[5].Value = list[i].Exams[3];
                dataTableStudent.Rows[i].Cells[6].Value = list[i].Exams[4];
                dataTableStudent.Rows[i].Cells[7].Value = Student.CounterFailedExam(list[i].Exams);
            }
        }

        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {

            FormAdd search = new FormAdd(FormState.SEARCH);
            search.ShowDialog();
            if (search.okClose)
            {
                int id = search.Student.ID;
                Student student = table.Find(id);
                if (student != null)
                {
                    FormAdd show = new FormAdd(FormState.DISPLAY, student);
                    List<string> list = student.Exams;
                    MessageBox.Show($"{student.FullName}\nкол-во несданных предметов: {Student.CounterFailedExam(student.Exams).ToString()} ");
                    show.ShowDialog();

                }
                else
                {
                    MessageBox.Show("Студент не найден!");
                }
            }
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdd formData = new FormAdd(FormState.ADD);
            formData.ShowDialog();
            if (formData.okClose) 
            { 
                student = formData.GetStudent();
                if (table.Add(student))
                {
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Данные не добавлены");
                }
            }
        }
        private void editDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdd search = new FormAdd(FormState.SEARCH);
            search.ShowDialog();
            if (search.okClose)
            {
                int id = search.Student.ID;
                Student student = table.Find(id);
                if (student != null)
                {
                    FormAdd edit = new FormAdd(FormState.EDIT, student);
                    edit.ShowDialog();
                    if (edit.okClose) 
                    { 
                        table.Delete(id);
                        table.Add(edit.GetStudent());
                        Redraw();
                    }
                }
                else
                {
                    MessageBox.Show("Студент не найден!");
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAdd form = new FormAdd(FormState.SEARCH);
            form.ShowDialog();
            if (form.okClose)
            {
                int id = form.Student.ID;
                if (student != null)
                {
                    table.Delete(id);
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Студент не найден");
                }
            }
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            openFileDialog.Filter = "Json File|*.json";
            openFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(openFileDialog.FileName))
            {
                FileName = openFileDialog.FileName;
                string txt = File.ReadAllText(FileName);
                List<Student> students = JsonConvert.DeserializeObject<List<Student>>(txt);
                table.Clear();
                foreach (Student student in students)
                {
                    table.Add(student);
                }
                Redraw();
            }
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FileName))
            {
                SaveToFile();
            }
            else
            {
                saveAsToolStripMenuItem_Click(sender, e);
            }
        }

        private void SaveToFile()
        {
            List<Student> students = table.GetData();
            string result = JsonConvert.SerializeObject(students);
            File.WriteAllText(FileName, result);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Json File|*.json";
            saveFileDialog.ShowDialog();
            if (!string.IsNullOrEmpty(saveFileDialog.FileName))
            {
                FileName = saveFileDialog.FileName;
                SaveToFile();
            }
        }

    }
}
