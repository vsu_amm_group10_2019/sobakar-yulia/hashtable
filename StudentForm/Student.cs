﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentForm
{
   public class Student
    {
        public string FullName { get; set; }
        public int ID { get; set; }
        public List<string> Exams { get; set; }  


        public Student(string editFullName, int editID, List<string> list)
        {
            FullName = editFullName;
            ID = editID;
            Exams = list;
        }
        
        public Student()
        {

        }
        public static int CounterFailedExam(List<string> exam)
           {
               int counter = 0;
               foreach (string elem in exam)
               {
                   if (elem == "2" || elem == "незачет")
                       counter++;
               }
               return counter;
           } 


    }
}
